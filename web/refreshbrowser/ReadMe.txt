-- About:
RefreshBrowser is a plug-in for Notepad++ which will refresh any open web browsers when a document in Notepad++ is saved. This relieves web developers from having to switch back and forth between Notepad++ and a web browser to check the changes they've made.

-- Homepage:
http://code-section.com/RefreshBrowser/
