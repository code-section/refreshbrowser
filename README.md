# README #

RefreshBrowsr is a plug-in for Notepad++ which will refresh any open web browsers when a document is saved, allowing web developers to instantly preview their work as soon as they save without having to switch back and forth between browser and NPP.

The code uses CSTB (code-section's toolbox): https://bitbucket.org/code-section/cstb

Visit the plug-in's homepage at:
http://code-section.com/page/refreshbrowser