RefreshBrowser 1.1

-- About:
RefreshBrowser is a plug-in for Notepad++ which will refresh any open web browsers when a document in Notepad++ is saved. This relieves web developers from having to switch back and forth between Notepad++ and a web browser to check the changes they've made.

RefreshBrowser is free for commercial and non-commercial use, but if you find it useful, we would appreciate it if you pay as much as you want for it. Use the "donate" button on the plugin's homepage.

-- Homepage:
http://code-section.com/RefreshBrowser/

-- Changes
1.1:
* Added support for Microsoft Edge browser.
* Added 64-bit version.

-- Support:
cm@code-section.com
