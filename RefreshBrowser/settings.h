// Plugin settings

# ifndef RB_SETTINGS_H
# define RB_SETTINGS_H


namespace RB
{


struct Settings
{
	bool bEnabled;

	bool bRefreshFirefox;
	bool bRefreshOpera;
	bool bRefreshChrome;
	bool bRefreshIE;
	bool bRefreshEdge;
	bool bIgnoreMinimized;
	bool bDisableWhileMaximized;

	int delay;

	int refreshCount;


	void Reset()
	{
		bEnabled = true;
		bRefreshFirefox = bRefreshOpera = bRefreshChrome = bRefreshIE = bRefreshEdge = true;
		bDisableWhileMaximized = false;
		bIgnoreMinimized = true;
		delay = 100;
	}

	Settings() { refreshCount = 0; Reset(); }
	
	bool Serialize( bool bLoad );
};

};


# endif