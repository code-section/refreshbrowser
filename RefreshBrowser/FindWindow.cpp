# include <Windows.h>
# include <CSTB\Win32\Win32Util.h>
# include <tchar.h>



struct HWND_LPCSTR
{
	HWND hWnd;
	LPCTSTR sName;
};




BOOL CALLBACK FindWindowByPartialNameCB( HWND hWnd, LPARAM lParam )
{
	HWND_LPCSTR* pWndStr = (HWND_LPCSTR*)lParam;

	TCHAR sName[ 1024 ];
	sName[0] = 0;
	GetWindowText( hWnd, sName, 1024 );

	for( unsigned i=0; i<1024 && sName[i] != 0; i++ )
		sName[i] = _totupper( sName[i] );
	
	//int i = FindSubstring( sName, 1024, L"FIREFOX", -1 );
	int i = FindSubstring( sName, 1024, pWndStr->sName, -1 );
	if( i > 0 )
	{
		pWndStr->hWnd = hWnd;
		return FALSE;
	}
	return TRUE;
}




HWND FindWindowByPartialName( LPCTSTR sName )
{
	HWND_LPCSTR ws;
	ws.sName = sName;
	ws.hWnd = NULL;
	EnumWindows( FindWindowByPartialNameCB, (LPARAM)&ws );
	return ws.hWnd;
}