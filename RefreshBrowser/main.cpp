/** The main module of the RefreshBrowser plugin for Notepad++.
Contains the entry point function.

Adel Amro
http://code-section.com
*/



# include "RefreshBrowser.h"
# include "settings.h"
# include <CSTB\Win32\Win32Util.h>
# include <stdio.h>
# include <tchar.h>



namespace RB
{
	FuncItems	funcItems;
	NppData		nppData = {0};
	Settings	settings;
	HINSTANCE	hInstance = NULL;

	bool		canSave = false;

	FuncItems&	GetFuncItems() { return funcItems; }
	NppData&	GetNppData() { return nppData; }
	Settings&	GetSettings() { return settings; }
	HINSTANCE	HInstance() { return hInstance; }
};




void ToggleEnable()
{
	RB::GetSettings().bEnabled = !RB::GetSettings().bEnabled;

	// Toggle the corresponding menu item.
	SendMessage( RB::NppWnd(),
				NPPM_SETMENUITEMCHECK,
				RB::GetFuncItems()[0]._cmdID,
				(LPARAM)RB::GetSettings().bEnabled );
}




BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  reasonForCall, 
                       LPVOID lpReserved )
{
    switch (reasonForCall)
    {
      case DLL_PROCESS_ATTACH:
		  // initialize the plugin here.
		  RB::hInstance = (HINSTANCE)hModule;
        break;

      case DLL_PROCESS_DETACH:
		  // cleanup the plugin here.
        break;

      case DLL_THREAD_ATTACH:
        break;

      case DLL_THREAD_DETACH:
        break;
    }

    return TRUE;
}




// Good place to initialize.
extern "C" __declspec(dllexport) void setInfo(NppData _nppData )
{
	RB::nppData = _nppData;

	// load settings.
	RB::GetSettings().Serialize( true );


	// Setup the func items.
	FuncItem fiToggle = {0};
	_tcsncpy( fiToggle._itemName, TEXT( "Toggle RefreshBrowser" ), ARRAY_SIZE(fiToggle._itemName) );
	fiToggle._pFunc = ToggleEnable;
	fiToggle._init2Check = RB::GetSettings().bEnabled;
	RB::GetFuncItems().push_back( fiToggle );

	FuncItem fiSettings = {0};
	_tcsncpy( fiSettings._itemName, TEXT("Settings..."), ARRAY_SIZE(fiSettings._itemName ) );
	fiSettings._pFunc = RB::ShowSettingsDlg;
	RB::GetFuncItems().push_back( fiSettings );
}




// Tell NPP about the plugin's commands
extern "C" __declspec(dllexport) FuncItem * getFuncsArray(int *nbF)
{
	*nbF = RB::GetFuncItems().size();
	if( RB::GetFuncItems().empty() )
		return NULL;
	return &RB::GetFuncItems()[0];
}




void CALLBACK RefreshTimerProc( HWND hWnd, UINT Msg, UINT_PTR idEvent, DWORD dwTime );



void EnableRefreshTimer( bool bEnable )
{
	static UINT_PTR timerID = -165;
	static bool enabled = false;

	if( bEnable && RB::GetSettings().delay < USER_TIMER_MINIMUM )
	{
		// If the delay is 0, just call the timer proc and don't set up any timer.
		RefreshTimerProc( NULL, WM_TIMER, 0, 0 );
		return;
	}

	if( bEnable == enabled )
		return;

	enabled = bEnable;

	if( bEnable )
	{
		timerID = SetTimer( NULL, 0, RB::GetSettings().delay, RefreshTimerProc );
	}
	else
	{
		KillTimer( NULL, timerID );
		timerID = -1;
	}
}




void CALLBACK RefreshTimerProc( HWND hWnd, UINT Msg, UINT_PTR idEvent, DWORD dwTime )
{
	RB::RefreshBrowsers();
	EnableRefreshTimer( false );
}



void RB::RefreshBrowsers()
{
	if( RB::GetSettings().bDisableWhileMaximized && IsMaximized( RB::NppWnd() ) )
		return;

	std::vector< HWND > windows;

	HWND hBrowser = NULL;
	if( RB::GetSettings().bRefreshFirefox && (hBrowser = FindWindowByPartialName( TEXT("FIREFOX") ) ))
		windows.push_back( hBrowser );
	if( RB::GetSettings().bRefreshOpera && (hBrowser = FindWindowByPartialName( TEXT("OPERA") ) ) )
		windows.push_back( hBrowser );
	if( RB::GetSettings().bRefreshChrome && (hBrowser = FindWindowByPartialName( TEXT("CHROME") ) ) )
		windows.push_back( hBrowser );
	if( RB::GetSettings().bRefreshIE && (hBrowser = FindWindowByPartialName( TEXT("INTERNET EXPLORER") ) ) )
		windows.push_back( hBrowser );
	if (RB::GetSettings().bRefreshEdge && (hBrowser = FindWindowByPartialName(TEXT("MICROSOFT EDGE"))))
		windows.push_back(hBrowser);

	if( windows.empty() )
		return;



	bool bRefreshed = false;

	for( unsigned i=0; i<windows.size(); i++ )
	{
		hBrowser = windows[i];
		if( RB::GetSettings().bIgnoreMinimized && IsMinimized( hBrowser ) )
			continue;

		bRefreshed = true;
		SetForegroundWindow( hBrowser );

		INPUT input = {0};
		input.type = INPUT_KEYBOARD;
		input.ki.wVk = VK_F5;

		Sleep( 50 );
		SendInput( 1, &input, sizeof(INPUT) );
		Sleep( 50 );
	}

	if( bRefreshed )
	{
		SetForegroundWindow( RB::NppWnd() );

		RB::GetSettings().refreshCount++;
		if( RB::GetSettings().refreshCount == 100 )
		{
			// Display a one-time message to ask for a donation.
			if( IDYES == MessageBox( RB::NppWnd(),
				TEXT(
"This is a one-time-only message to inform you that you have used RefreshBrowser 100 times.\r\n\
Would you please consider making a donation to support the development of more free software?" ),
				PLUGIN_NAME,
				MB_YESNO | MB_ICONEXCLAMATION ) )
			{
				ShellExecute( RB::NppWnd(), TEXT("open"),
					TEXT("http://code-section.com/refreshbrowser/#donate"), NULL, NULL, SW_SHOWNORMAL );
			}
		}
	}
}




extern "C" __declspec(dllexport) void beNotified(SCNotification *notifyCode)
{
	switch (notifyCode->nmhdr.code) 
	{
		case NPPN_SHUTDOWN:
			// Save settings
			RB::GetSettings().Serialize( false );
			break;


		case NPPN_FILESAVED:
			RB::canSave = false;
			if( RB::GetSettings().bEnabled )
			{
				EnableRefreshTimer( true );
			}
			break;


		case NPPN_TBMODIFICATION:
			{
				toolbarIcons tbIcons = {0};
				tbIcons.hToolbarBmp = (HBITMAP)::LoadImage( RB::HInstance(), MAKEINTRESOURCE( IDB_TBICON ),
														IMAGE_BITMAP, 0, 0,
														(LR_LOADTRANSPARENT | LR_DEFAULTSIZE | LR_LOADMAP3DCOLORS) );
				tbIcons.hToolbarIcon = LoadIcon( RB::HInstance(), MAKEINTRESOURCE(IDI_TBICON) );
				::SendMessage( RB::GetNppData()._nppHandle, NPPM_ADDTOOLBARICON,
							(WPARAM)RB::funcItems[0]._cmdID, (LPARAM)&tbIcons );
			}
			break;


		case SCN_SAVEPOINTLEFT:
			RB::canSave = true;
			//PlaySound(TEXT("SystemStart"), NULL, SND_ALIAS);
			break;

		default:
			return;
	}
}



extern "C" __declspec(dllexport) const TCHAR * getName() { return PLUGIN_NAME; }

extern "C" __declspec(dllexport) LRESULT messageProc(UINT Message, WPARAM wParam, LPARAM lParam)
{ return TRUE; }

#ifdef UNICODE
extern "C" __declspec(dllexport) BOOL isUnicode() { return TRUE; }
#endif //UNICODE
