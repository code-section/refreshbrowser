# include "RefreshBrowser.h"
# include "Settings.h"
# include <CSTB\Win32\Dialog.h>
# include <CSTB\Win32\Win32Util.h>
# include <stdio.h>
# include <tchar.h>
# include <strsafe.h>




BOOL IntFromEdit( HWND hEdit, INT& num )
{
	if( !hEdit ) return FALSE;
	TCHAR s[8] = {0};
	INT len = GetWindowText( hEdit, s, ARRAY_SIZE(s) );
	if( len == 0 )
		return FALSE;
	return 1 == _sntscanf_s( s, len, TEXT("%d"), &num );
}




BOOL IntToEdit( HWND hEdit, INT num )
{
	if( !hEdit )
		return FALSE;
	TCHAR s[8] = {0};
	StringCchPrintf( s, ARRAY_SIZE(s), TEXT("%d"), num );
	//SetWindowText( hEdit, s );
	SendMessage( hEdit, WM_SETTEXT, 0, (LPARAM)s );
	return TRUE;
}




namespace RB
{


class SettingsDialog : public CSTB::Dialog
{
public:
	
	void UpdateSettings()
	{
		RB::Settings& settings = RB::GetSettings();

		settings.bRefreshFirefox = Button_GetCheck( GetDlgItem( hWnd, IDC_REFRESH_FIREFOX ) ) == BST_CHECKED;
		settings.bRefreshOpera = Button_GetCheck( GetDlgItem( hWnd, IDC_REFRESH_OPERA ) ) == BST_CHECKED;
		settings.bRefreshChrome = Button_GetCheck( GetDlgItem( hWnd, IDC_REFRESH_CHROME ) ) == BST_CHECKED;
		settings.bRefreshIE = Button_GetCheck( GetDlgItem( hWnd, IDC_REFRESH_IE ) ) == BST_CHECKED;
		settings.bRefreshEdge = Button_GetCheck(GetDlgItem(hWnd, IDC_REFRESH_EDGE)) == BST_CHECKED;
		settings.bIgnoreMinimized = Button_GetCheck( GetDlgItem( hWnd, IDC_IGNORE_MINIMIZED ) ) == BST_CHECKED;
		settings.bDisableWhileMaximized = Button_GetCheck( GetDlgItem( hWnd, IDC_DISABLE_MAXIMIZED ) ) == BST_CHECKED;
		IntFromEdit( GetDlgItem( hWnd, IDC_DELAY ), settings.delay );
	}



	VOID UpdateControls()
	{
		RB::Settings& settings = RB::GetSettings();

		Button_SetCheck( GetDlgItem( hWnd, IDC_REFRESH_FIREFOX ), settings.bRefreshFirefox );
		Button_SetCheck( GetDlgItem( hWnd, IDC_REFRESH_OPERA ), settings.bRefreshOpera );
		Button_SetCheck( GetDlgItem( hWnd, IDC_REFRESH_CHROME ), settings.bRefreshChrome );
		Button_SetCheck( GetDlgItem( hWnd, IDC_REFRESH_IE ), settings.bRefreshIE );
		Button_SetCheck(GetDlgItem(hWnd, IDC_REFRESH_EDGE), settings.bRefreshEdge);
		Button_SetCheck( GetDlgItem( hWnd, IDC_IGNORE_MINIMIZED ), settings.bIgnoreMinimized );
		Button_SetCheck( GetDlgItem( hWnd, IDC_DISABLE_MAXIMIZED ), settings.bDisableWhileMaximized );

		IntToEdit( GetDlgItem( hWnd, IDC_DELAY ), settings.delay );
	}


	BOOL OnInitDialog()
	{
		UpdateControls();
		SetWindowText( GetDlgItem( hWnd, IDC_ABOUT ),
			PLUGIN_NAME TEXT(" ") PLUGIN_VERSION );
		return TRUE;
	}

	//BOOL OnClose( OnCommand( NULL, IDCANCEL, 0 ); }


	LRESULT OnCommand( HWND hCtrl, WORD id, WORD code )
	{
		switch( id )
		{
		case IDC_HOMEPAGE:
			ShellExecute( RB::NppWnd(), TEXT("open"),
				TEXT("http://code-section.com/refreshbrowser/#homepagebutton"), NULL, NULL, SW_SHOWNORMAL );
			return 0;


		case IDOK:
			UpdateSettings();
			// fall through
		case IDCANCEL:
			DestroyWindow( hWnd );
			hWnd = NULL;
			return 0;
		}
		return CSTB::Dialog::OnCommand( hCtrl, id, code );
	}
};



void ShowSettingsDlg()
{
	static SettingsDialog settingsDlg;
	if( settingsDlg.hWnd )
	{
		SetActiveWindow( settingsDlg.hWnd );
		return;
	}
	settingsDlg.Create( RB::HInstance(), RB::NppWnd(), MAKEINTRESOURCE( IDD_SETTINGS ) );
}


}; // namespace RB
