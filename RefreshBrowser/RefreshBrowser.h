/** The main header file for the RefreshBrowser Notepad++ plugin.

Adel Amro
http://code-section.com
**/

# ifndef REFRESH_BROWSER_H
# define REFRESH_BROWSER_H


# include "PluginInterface.h"
# include <windowsx.h>
# include <vector>
# include "resource.h"


# define PLUGIN_NAME		TEXT("RefreshBrowser")
# define PLUGIN_VERSION		TEXT("1.1")

namespace RB
{

typedef std::vector< FuncItem > FuncItems;
struct Settings;

FuncItems&		GetFuncItems();
NppData&		GetNppData();
Settings&		GetSettings();
HINSTANCE		HInstance();
inline HWND		NppWnd() { return GetNppData()._nppHandle; }

void			RefreshBrowsers();
void			ShowSettingsDlg();

}; // namespace RB


// Utility stuff

// The name should be in upper case.
HWND FindWindowByPartialName( LPCTSTR sName );


# endif