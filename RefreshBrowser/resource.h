#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDD_SETTINGS                            101
#define IDB_TBICON                              105
#define IDI_TBICON                              107
#define IDC_ABOUT                               1000
#define IDC_IGNORE_MINIMIZED                    1001
#define IDC_DELAY                               1007
#define IDC_REFRESH_FIREFOX                     1009
#define IDC_REFRESH_OPERA                       1010
#define IDC_REFRESH_CHROME                      1011
#define IDC_REFRESH_IE                          1012
#define IDC_HOMEPAGE                            1013
#define IDC_EXTENSIONS                          1015
#define IDC_REFRESH_EDGE                        40000
#define IDC_DISABLE_MAXIMIZED                   40001
