# include "settings.h"
# include "RefreshBrowser.h"
# include <CSTB\tinyxml2\XML.h>
# include <CSTB\win32\Win32Util.h>
# include <Shlwapi.h>


namespace RB
{


bool Settings::Serialize( bool bLoad )
{
	TCHAR sFile[ MAX_PATH ];
	sFile[0] = 0;

	SendMessage( NppWnd(), NPPM_GETPLUGINSCONFIGDIR, ARRAY_SIZE( sFile ), (LPARAM)sFile );
	if( sFile[0] == 0 )
		return false;

	PathAppend( sFile, TEXT("RefreshBrowserCfg.xml") );
	
	XML::Document doc;
	if( !doc.Load( sFile ) && bLoad )
		return false;

	XML::HELEMENT hSettings = doc.FindOrCreateElement( "settings" );
	hSettings.GetSetAttribute( "enabled", bLoad, bEnabled );
	hSettings.GetSetAttribute( "doFirefox", bLoad, bRefreshFirefox );
	hSettings.GetSetAttribute( "doOpera", bLoad, bRefreshOpera );
	hSettings.GetSetAttribute( "doChrome", bLoad, bRefreshChrome );
	hSettings.GetSetAttribute( "doIE", bLoad, bRefreshIE );
	hSettings.GetSetAttribute("doEdge", bLoad, bRefreshEdge);
	hSettings.GetSetAttribute( "delay", bLoad, delay );
	hSettings.GetSetAttribute( "ignoreMinimized", bLoad, bIgnoreMinimized );
	hSettings.GetSetAttribute( "disableWhileMaximized", bLoad, bDisableWhileMaximized );
	hSettings.GetSetAttribute( "rc", bLoad, refreshCount );

	if( !bLoad )
		doc.Save( sFile );

	return true;
}

};